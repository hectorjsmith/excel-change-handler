---
title: Documentation
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<span class="badge-placeholder">[![Project](https://img.shields.io/badge/project-gitlab-brightgreen?style=flat&logo=gitlab)](https://gitlab.com/hectorjsmith/excel-change-handler/)</span>
<span class="badge-placeholder">[![Build Status](https://gitlab.com/hectorjsmith/excel-change-handler/badges/main/pipeline.svg)](https://gitlab.com/hectorjsmith/excel-change-handler/commits/main)</span>
<span class="badge-placeholder">[![Nuget package](https://badgen.net/nuget/v/ExcelChangeHandler/latest)](https://www.nuget.org/packages/ExcelChangeHandler/)</span>
<span class="badge-placeholder">[![License: MIT](https://img.shields.io/badge/license-MIT-brightgreen)](https://gitlab.com/hectorjsmith/excel-change-handler/-/blob/main/LICENSE)</span>

<!-- markdownlint-restore -->
# Excel Change Handler
*C# library to enable detecting and processing changes made on an Excel worksheet*

![Gif of change handler highlighting changes automatically](img/changeHandler.gif)

The gif above shows how the library can be used to automatically highlight changes made to a workbook. These highlights are then saved in the workbook to facilitate reviews by other people.

